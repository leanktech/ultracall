package com.ultralab.ultracall.activity.bean

import java.io.Serializable
import java.util.*

/**
 * @ClassName: CallErrorBean
 * @Description: 初始化twilio 错误信息收集
 * @Author: jason@leanktech.com
 * @Date: 2021/8/24 15:47
 */
class CallErrorBean(
    var errorTitle: String = "Call Error",
    var errorCode: Int = 0,
    var errorMessage: String = ""
) : Serializable {

    fun toStringMessage(): String = String.format(
        Locale.getDefault(),
        "$errorTitle: %d, %s",
        errorCode,
        errorMessage
    )
}