package com.ultralab.ultracall.activity.ad_activity.viewmodel

import androidx.lifecycle.MutableLiveData
import com.ultralab.base_lib.base.BaseViewMode
import com.ultralab.base_lib.tool.ActivityMgr
import com.ultralab.base_lib.tool.init
import com.ultralab.ultracall.tools.NetWorkUtils
import com.ultralab.ultracall.tools.checkTodayCheckInBoolean
import com.ultralab.ultracall.tools.getDeviceCountryCode
import com.ultralab.ultracall.tools.getSystemModelDeviceBrand
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


/**
 * @Description: 展示
 * @Author: jason@leanktech.com
 * @Date: 2021/10/12 11:16
 */
class NativeAdViewModel : BaseViewMode() {

    /**
     * 国家地区
     */
    val country = MutableLiveData<String>().init("")

    /**
     * 手机品牌
     */
    val phoneBrand = MutableLiveData<String>().init("")

    /**
     * ip地址
     */
    val ipString = MutableLiveData<String>().init("")

    /**
     * 用户默认头像
     */
    val userDefaultImage = MutableLiveData<String>().init("")

    /**
     * 签到显示
     */
    val checkInToday = MutableLiveData<Boolean>().init(false)


    fun loadInfoData() {
        //国家
        country.value = getDeviceCountryCode(ActivityMgr.getContext())?:""
        //手机型号
        phoneBrand.value = getSystemModelDeviceBrand()
        //获取ip地址
        ipString.value = NetWorkUtils.getIpAddress(ActivityMgr.getContext().applicationContext)
        //显示签到
        checkInToday.value = checkTodayCheckInBoolean()
    }

    /**
     * 获取公网ip方法
     * @return "var returnCitySN = {"cip": "111.194.154.115", "cid": "110000", "cname": "北京市"};"
     */
    fun getPublicNetworkIp() {
        CoroutineScope(Dispatchers.IO).launch {
//            val defaultRetrofit = RetrofitClient.getDefaultRetrofit(UrlPath.PUBLIC_NETWORK_IP)
//            val create = defaultRetrofit.create(AdService::class.java)
//            val netWorkIp = create.getNetWorkIp()
//            netWorkIp?.enqueue(object: Callback<String> {
//                override fun onResponse(call: Call<String>, response: Response<String>) {
//                  "返回结果 ${response.message()}".printError()
//                }
//
//                override fun onFailure(call: Call<String>, t: Throwable) {
//                    "请求错误 ${t.message}".printError()
//                }
//            })
        }
    }
}