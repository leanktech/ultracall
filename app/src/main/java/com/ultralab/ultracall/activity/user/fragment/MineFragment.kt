package com.ultralab.ultracall.activity.user.fragment

import android.content.Intent
import android.view.View
import com.jeremyliao.liveeventbus.LiveEventBus
import com.ultralab.base_lib.fragment.BaseFragment
import com.ultralab.base_lib.tool.ActivityMgr
import com.ultralab.base_lib.tool.print
import com.ultralab.ultracall.LiveEventData
import com.ultralab.ultracall.R
import com.ultralab.ultracall.activity.ad_activity.NativeAdActivity
import com.ultralab.ultracall.activity.user.viewmodel.MineViewModel
import com.ultralab.ultracall.databinding.MineFragmentBinding
import com.ultralab.ultracall.local.AccountBean
import com.ultralab.ultracall.tools.*
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @ClassName: MineFragment
 * @Description: 我的页面
 * @Author: jason@leanktech.com
 * @Date: 2021/8/26 16:48
 */
class MineFragment : BaseFragment<MineFragmentBinding>() {

    /** 是否点击过 广告 */
    private var checkClickAd = false

    private val mViewModel by viewModel<MineViewModel>()

    override fun getLayoutId(): Int = R.layout.mine_fragment

    override fun initView() {
        bindingView.vm = mViewModel
        bindingView.lifecycleOwner = this
        bindingView.presenter = this
        bindingView.ab = AccountBean

        mViewModel.checkAdIndex.observe(this, {
            if (checkClickAd) {
                rewardForWatchingAdvertisements(it)
            }
        })

        //监听原生广告结束后，更新credits
        LiveEventBus.get(LiveEventData.UP_CREDITS, Boolean::class.java).observe(this, {
            if (it) {
                if (!checkTodayCheckInBoolean()) {
                    earnPoints()
                }
                //用户手动改时间证明有意愿玩这个东西。开发在初始化一次广告
                AdUtils.loadSignInAd(ActivityMgr.getContext().applicationContext)
                AdUtils.loadSignInNativeAd(ActivityMgr.getContext().applicationContext)
            }
        })
    }

    override fun loadData(isRefresh: Boolean) {
        mViewModel.getNetWorkTimeZone()
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                //买次数
                R.id.by_frequency_view -> {
                    //"点击".showToast()
                }
                //签到
                R.id.check_in_view -> {
                    checkInFun()
                }
                R.id.ad_1_view -> {
                    checkClickAd = true
                    mViewModel.checkAdIndex.value = 1
                }
                R.id.ad_2_view -> {
                    checkClickAd = true
                    mViewModel.checkAdIndex.value = 2
                }
                R.id.ad_3_view -> {
                    checkClickAd = true
                    mViewModel.checkAdIndex.value = 3
                }
            }
        }
    }

    /**
     * 签到成功弹窗以及失败
     */
    private fun checkInFun() {
        AnalyticsUtils.clickNativeAd()
        if (AdUtils.signInAd.value != null) {
            showSignInAd(requireActivity()) {
                showAdView {
                    mViewModel.showAd.value = true
                }
                adDisMiss {
                    mViewModel.showAd.value = false
                    startActivity(Intent(requireContext(), NativeAdActivity::class.java))
                }
            }
        } else {
            if (AdUtils.primitiveAd.value != null) {
                startActivity(Intent(requireContext(), NativeAdActivity::class.java))
            }
        }
    }

    /**
     * 获得积分
     */
    private fun earnPoints() {
        AccountBean.adCredits = AccountBean.adCredits + mViewModel.checkInReward.value!!
        getWithNetWorkTime()
    }

    /**
     * 点击广告做事情
     * 通过下标得到对应奖励
     * @param index ad 下标
     */
    private fun rewardForWatchingAdvertisements(index: Int) {
        if (index > 0 && AdUtils.rewardedVideo.value != null) {
            showRewardedAd(requireActivity()) {
                adDisMiss {
                    mViewModel.showAd.value = false
                    //tod0
                    val number = when (index) {
                        1 -> {
                            AccountBean.luckAdFrequency++
                            val luckAd = when (AccountBean.luckAdFrequency) {
                                in 1..5 -> getRandom(30, 10)
                                in 6..10 -> getRandom(35, 20)
                                in 11..20 -> getRandom(40, 1)
                                else -> 0
                            }
                            luckAd
                        }
                        2 -> {
                            AccountBean.fortuneBagAdFrequency++
                            val fortuneBagAd = when (AccountBean.fortuneBagAdFrequency) {
                                in 1..3 -> getRandom(65, 50)
                                in 4..5 -> getRandom(110, 50)
                                in 6..10 -> getRandom(65, 50)
                                else -> 0
                            }
                            fortuneBagAd
                        }
                        3 -> {
                            AccountBean.fortuneBagAdFrequency++
                            val fortuneBagAd = when (AccountBean.fortuneBagAdFrequency) {
                                in 1..2 -> getRandom(30, 10)
                                3 -> getRandom(75, 55)
                                in 4..10 -> getRandom(20, 10)
                                in 11..15 -> getRandom(30, 1)
                                in 17..20 -> getRandom(20, 1)
                                else -> 0
                            }
                            fortuneBagAd
                        }
                        else -> 0
                    }
                    "获得多少奖励 $number".print("random")
                }
                showAdView {
                    mViewModel.showAd.value = true
                }
                rewardItem {
                    //返回广告奖励
                }
            }
        }
    }
}