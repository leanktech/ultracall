package com.ultralab.ultracall.activity.bean

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.ultralab.ultracall.BR
import java.io.Serializable

/**
 * @author mazeyu
 * @email mazeyu8665@dingtalk.com
 * @date 2021/3/16
 * @since 1.0.0
 * @version 1.0.0
 */
class EmptyItemBean : BaseObservable(), Serializable {

    /**
     * item高度
     */
    @set:Bindable
    var itemHeight: Int = 0
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.itemHeight)
        }

    /**
     * item宽度
     */
    @set:Bindable
    var itemWidth: Int = 0
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.itemWidth)
        }

    /**
     * item是否开始展示
     */
    var show = false

    /**
     * item是否展示完毕
     */
    var showComplete = false
}