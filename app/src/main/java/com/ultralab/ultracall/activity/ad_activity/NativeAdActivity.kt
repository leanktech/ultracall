package com.ultralab.ultracall.activity.ad_activity

import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.google.android.gms.ads.nativead.MediaView
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdView
import com.gyf.immersionbar.ImmersionBar
import com.jeremyliao.liveeventbus.LiveEventBus
import com.ultralab.base_lib.activity.BaseActivity
import com.ultralab.base_lib.tool.AppTool
import com.ultralab.base_lib.tool.StatusBarTool
import com.ultralab.base_lib.tool.print
import com.ultralab.ultracall.LiveEventData
import com.ultralab.ultracall.R
import com.ultralab.ultracall.activity.ad_activity.viewmodel.NativeAdViewModel
import com.ultralab.ultracall.databinding.NativeAdActiviyBinding
import com.ultralab.ultracall.local.AccountBean
import com.ultralab.ultracall.tools.AdUtils
import com.ultralab.ultracall.tools.AnalyticsUtils
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @Description: 展示
 * @Author: jason@leanktech.com
 * @Date: 2021/10/12 11:16
 */
class NativeAdActivity : BaseActivity<NativeAdActiviyBinding>() {

    private val mViewModel by viewModel<NativeAdViewModel>()

    override fun getLayoutId(): Int = R.layout.native_ad_activiy

    override val showToolBar: Boolean
        get() = false

    override fun initView() {
        bindingView.statusBar.layoutParams.height =
            StatusBarTool.getStatusBarHeight(bindingView.statusBar.context)
        ImmersionBar.with(this).transparentBar().init()

        bindingView.vm = mViewModel
        bindingView.lifecycleOwner = this
        bindingView.ac = AccountBean
        bindingView.presenter = this
        //设置状态栏高度
        bindingView.statusBar.layoutParams.height =
            StatusBarTool.getStatusBarHeight(bindingView.statusBar.context)
    }

    override fun loadData(isRefresh: Boolean) {
        //加载品牌信息
        mViewModel.loadInfoData()
        //加载网络ip
        mViewModel.getPublicNetworkIp()
        //加载广告
        val nativeAd = AdUtils.primitiveAd.value
        if (nativeAd != null) {
            AnalyticsUtils.openNativeAd()
            val nativeAdView = layoutInflater.inflate(R.layout.native_new_ad, null) as NativeAdView
            populateNativeAdView(nativeAd, nativeAdView)
            bindingView.adLayout.removeAllViews()
            bindingView.adLayout.addView(nativeAdView)
        } else {
            finish()
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        if (v != null) {
            AppTool.singleClick(v) {
                when (v.id) {
                    R.id.back_iv -> {
                        finish()
                    }
                }
            }
        }
    }

    /**
     * Populates a [NativeAdView] object with data from a given [NativeAd].
     *
     * @param nativeAd the object containing the ad's assets
     * @param adView the view to be populated
     */
    private fun populateNativeAdView(nativeAd: NativeAd, adView: NativeAdView) {
        // Set the media view. Media content will be automatically populated in the media view once
        // adView.setNativeAd() is called.
        val mediaView: MediaView = adView.findViewById(R.id.ad_media)
        adView.mediaView = mediaView

        val headLine = adView.findViewById(R.id.ad_headline) as TextView
        // Set other ad assets.
        adView.headlineView = headLine
        adView.bodyView = headLine
        adView.callToActionView = adView.findViewById(R.id.ad_call_to_action)
        adView.iconView = adView.findViewById(R.id.ad_app_icon)
        //adView.priceView = adView.findViewById(R.id.ad_price)
        //adView.starRatingView = adView.findViewById(R.id.ad_stars)
        //adView.storeView = adView.findViewById(R.id.ad_store)
        //adView.advertiserView = adView.findViewById(R.id.ad_advertiser)

        // The headline is guaranteed to be in every NativeAd.
        (adView.headlineView as TextView).text = nativeAd.headline

        // These assets aren't guaranteed to be in every NativeAd, so it's important to
        // check before trying to display them.
        if (nativeAd.body == null) {
            //adView.bodyView.visibility = View.INVISIBLE
        } else {
            //adView.bodyView.visibility = View.VISIBLE
            "nativeAd body ${nativeAd.body}".print()
            (adView.bodyView as TextView).text = nativeAd.body
        }
        if (nativeAd.callToAction == null) {
            adView.callToActionView.visibility = View.INVISIBLE
        } else {
            adView.callToActionView.visibility = View.VISIBLE
            (adView.callToActionView as Button).text = nativeAd.callToAction
        }
        if (nativeAd.icon == null) {
            adView.iconView.visibility = View.GONE
        } else {
            (adView.iconView as ImageView).setImageDrawable(
                nativeAd.icon.drawable
            )
            adView.iconView.visibility = View.VISIBLE
        }
//        if (nativeAd.price == null) {
//            adView.priceView.visibility = View.INVISIBLE
//        } else {
//            adView.priceView.visibility = View.VISIBLE
//            (adView.priceView as TextView).text = nativeAd.price
//        }
//        if (nativeAd.store == null) {
//            adView.storeView.visibility = View.INVISIBLE
//        } else {
//            adView.storeView.visibility = View.VISIBLE
//            (adView.storeView as TextView).text = nativeAd.store
//        }
//        if (nativeAd.starRating == null || nativeAd.starRating < 3) {
//            adView.starRatingView.visibility = View.INVISIBLE
//        } else {
//            (adView.starRatingView as RatingBar).rating = nativeAd.starRating.toFloat()
//            adView.starRatingView.visibility = View.VISIBLE
//        }
//        if (nativeAd.advertiser == null) {
//            adView.advertiserView.visibility = View.INVISIBLE
//        } else {
//            (adView.advertiserView as TextView).text = nativeAd.advertiser
//            adView.advertiserView.visibility = View.VISIBLE
//        }
        adView.setNativeAd(nativeAd)
    }

    override fun finish() {
        super.finish()
        if (AdUtils.primitiveAd.value != null) {
            "监听原生结束监听".print()
            AdUtils.primitiveAd.value?.destroy()
            AdUtils.signInAd.value = null
            AdUtils.primitiveAd.value = null
            LiveEventBus.get<Boolean>(LiveEventData.UP_CREDITS).post(true)
        }
    }
}