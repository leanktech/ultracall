package com.ultralab.ultracall.activity.user.viewmodel

import com.ultralab.base_lib.base.BaseViewMode

/**
 * @ClassName: LoginViewModel
 * @Description: 登录页面 P层
 * @Author: jason@leanktech.com
 * @Date: 2021/9/2 10:56
 */
class LoginViewModel : BaseViewMode() {
}