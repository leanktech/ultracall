package com.ultralab.ultracall.activity.home.viewmodel

import androidx.databinding.ObservableArrayList
import androidx.lifecycle.MutableLiveData
import com.ultralab.base_lib.base.BaseViewMode
import com.ultralab.base_lib.tool.init
import com.ultralab.ultracall.FirStoreTable
import com.ultralab.ultracall.FirStoreTableCollection
import com.ultralab.ultracall.StoreToKey
import com.ultralab.ultracall.activity.bean.DialListBean
import com.ultralab.ultracall.activity.bean.EmptyItemBean
import com.ultralab.ultracall.db.DbManger
import com.ultralab.ultracall.local.AccountBean
import com.ultralab.ultracall.tools.getDbJsonData

/**
 * @ClassName: CallViewModel
 * @Description: 类作用描述
 * @Author: jason@leanktech.com
 * @Date: 2021/8/26 18:22
 */
class CallViewModel : BaseViewMode() {

    /**
     * 显示loading 动画
     */
    val showLoading = MutableLiveData<Boolean>().init(false)

    val showLoadingTime = MutableLiveData<Boolean>().init(false)

    /**
     * 存储拨打记录列表
     */
    val dialList = ObservableArrayList<Any>()

    /**
     * 空Item最大高度
     */
    var emptyMaxHeight = 0

    fun loadData(isLoad: Boolean = false) {
        DbManger.getFireStore().collection(FirStoreTableCollection.TABLE_USERS)
            .document(FirStoreTable.TAB_USER)
            .get()
            .addOnSuccessListener {
                showLoadingTime.value = false
                //"Success${it.data?.toJson()} ".print()
                val dbJsonData =
                    it.data?.getDbJsonData(StoreToKey.KEY_CALL_RECORDS, DialListBean::class.java)
                //过滤不是当前账户下的user
                val newDbData = dbJsonData?.filter { filter ->
                    if (filter.id != 0L) {
                        filter.id == AccountBean.userId.toLong()
                    } else {
                        filter.temporaryId == AccountBean.userId.toLong()
                    }
                }
                if (isLoad) {
                    dialList.clear()
                }
                if (newDbData?.size ?: 0 == 0) {
                    dialList.add(EmptyItemBean().apply { itemHeight = emptyMaxHeight })
                } else {
                    newDbData?.forEach { dialListBean ->
                        dialList.add(dialListBean)
                    }
                }
            }.addOnFailureListener {
                showLoadingTime.value = false
                dialList.add(EmptyItemBean().apply { itemHeight = emptyMaxHeight })
            }
    }
}