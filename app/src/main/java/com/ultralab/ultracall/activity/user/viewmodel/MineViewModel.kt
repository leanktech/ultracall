package com.ultralab.ultracall.activity.user.viewmodel

import androidx.lifecycle.MutableLiveData
import com.ultralab.base_lib.base.BaseViewMode
import com.ultralab.base_lib.tool.init
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


/**
 * @ClassName: MineViewModel
 * @Description: 类作用描述
 * @Author: jason@leanktech.com
 * @Date: 2021/8/26 18:24
 */
class MineViewModel : BaseViewMode() {

    /**
     * 显示全屏广告背景
     */
    val showAd = MutableLiveData<Boolean>().init(false)

    /**
     * 获得签到奖励
     */
    val checkInReward = MutableLiveData<Int>().init(100)

    /**
     * 选择 ad 下标
     */
    val checkAdIndex = MutableLiveData<Int>().init(1)

    /**
     * 开放adview
     */
    val openAdView = MutableLiveData<Boolean>().init(true)

    fun getNetWorkTimeZone() {
        launchUI {
            withContext(Dispatchers.IO) {

            }
        }
    }

}