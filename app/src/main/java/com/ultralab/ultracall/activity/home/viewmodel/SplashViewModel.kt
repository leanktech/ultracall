package com.ultralab.ultracall.activity.home.viewmodel

import androidx.lifecycle.MutableLiveData
import com.ultralab.base_lib.base.BaseViewMode
import com.ultralab.base_lib.tool.init

/**
 * @Description: 开屏页面
 * @Author: jason@leanktech.com
 * @Date: 2021/8/23 15:51
 */
class SplashViewModel : BaseViewMode() {

    /**
     * 显示Loading进度图
     */
    val showLoadView = MutableLiveData<Boolean>().init(false)

    /**
     * 进度条总进度
     * 10s
     */
    val showTotalCount = MutableLiveData<Int>().init(1000)
    /**
     * loading 超时时间
     * 20s
     */
    val adLoadTimeOutTime = MutableLiveData<Int>().init(2000)
    /**
     * 显示loading进度值
     */
    val showLoadingCount = MutableLiveData<Int>().init(0)

    /**
     * 显示隐藏勾选状态
     */
    val checkState = MutableLiveData<Boolean>().init(false)

}