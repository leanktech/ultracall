package com.ultralab.ultracall.activity

import android.annotation.SuppressLint
import android.view.View
import com.alibaba.android.arouter.facade.annotation.Autowired
import com.alibaba.android.arouter.facade.annotation.Route
import com.gyf.immersionbar.ImmersionBar
import com.ultralab.base_lib.activity.BaseActivity
import com.ultralab.base_lib.tool.AppTool
import com.ultralab.base_lib.tool.StatusBarTool
import com.ultralab.ultracall.ARoutePath
import com.ultralab.ultracall.R
import com.ultralab.ultracall.databinding.H5ActivityBinding

/**
 * @ClassName: H5Activity
 * @Description: 加载h5页面
 * @Author: jason@leanktech.com
 * @Date: 2021/9/17 13:44
 */
@Route(path = ARoutePath.H5_PATH)
class H5Activity : BaseActivity<H5ActivityBinding>() {

    /**
     * H5页面url
     */
    @Autowired(name = "h5url")
    @JvmField
    var h5url: String = ""

    /**
     * H5页面标题
     */
    @Autowired(name = "h5title")
    @JvmField
    var h5title: String? = ""

    override val showToolBar: Boolean
        get() = false

    override fun getLayoutId(): Int = R.layout.h5_activity

    @SuppressLint("SetJavaScriptEnabled", "NewApi")
    override fun initView() {
        bindingView.statusBar.layoutParams.height =
            StatusBarTool.getStatusBarHeight(bindingView.statusBar.context)
        ImmersionBar.with(this).statusBarView(bindingView.statusBar).init()
        bindingView.lifecycleOwner = this
        bindingView.presenter = this

        bindingView.x5WebView.settings.apply {
            javaScriptEnabled = true
            javaScriptCanOpenWindowsAutomatically = true
            setSupportZoom(true)
            displayZoomControls = false
            allowContentAccess = true
            domStorageEnabled = true
            mediaPlaybackRequiresUserGesture = true
            loadsImagesAutomatically = true

        }
    }

    override fun loadData(isRefresh: Boolean) {
        if (h5url.isEmpty()) {
            h5url = intent?.getStringExtra("h5url") ?: ""
        }
        bindingView.x5WebView.loadUrl(h5url)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        if (v != null) {
            AppTool.singleClick(v) {
                when (v.id) {
                    R.id.close_iv -> {
                        finish()
                    }
                }
            }
        }
    }
}