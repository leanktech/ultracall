package com.ultralab.ultracall.activity.home.activity

import android.Manifest
import android.content.Intent
import android.graphics.Paint
import android.view.View
import androidx.lifecycle.viewModelScope
import com.alibaba.android.arouter.facade.annotation.Route
import com.gyf.immersionbar.BarHide
import com.gyf.immersionbar.ImmersionBar
import com.hjq.permissions.OnPermissionCallback
import com.hjq.permissions.XXPermissions
import com.jeremyliao.liveeventbus.LiveEventBus
import com.ultralab.base_lib.activity.BaseActivity
import com.ultralab.base_lib.tool.AppTool
import com.ultralab.base_lib.tool.showToast
import com.ultralab.ultracall.*
import com.ultralab.ultracall.activity.H5Activity
import com.ultralab.ultracall.activity.home.viewmodel.SplashViewModel
import com.ultralab.ultracall.databinding.SplashActivityBinding
import com.ultralab.ultracall.local.AccountBean
import com.ultralab.ultracall.tools.AdUtils
import com.ultralab.ultracall.tools.equipmentId
import com.ultralab.ultracall.tools.isAppOpenAdAvailable
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @ClassName: SplashActivity
 * @Description: 开屏页面
 * @Author: jason@leanktech.com
 * @Date: 2021/8/23 15:51
 */
@Route(path = ARoutePath.ROUTE_SPLASH_PATH)
class SplashActivity : BaseActivity<SplashActivityBinding>() {

    private val mViewModel by viewModel<SplashViewModel>()

    //隐私与政策状态
    private var checkState = false

    override fun getLayoutId(): Int = R.layout.splash_activity

    //默认=false走冷启动流程 =true走热启动弹出app open ad
    private val checkInit by lazy {
        intent?.getBooleanExtra("checkInit", false) ?: false
    }

    override fun initView() {
        ImmersionBar.with(this).hideBar(BarHide.FLAG_HIDE_BAR).fullScreen(true).init()
        bindingView.lifecycleOwner = this
        bindingView.presenter = this
        bindingView.vm = mViewModel
        bindingView.ab = AccountBean
        //只有在冷启动显示权限初始化ad等
        if (!checkInit) {
            bindingView.privacyPolicyTv.paint.flags = Paint.UNDERLINE_TEXT_FLAG
            bindingView.userAgreementTv.paint.flags = Paint.UNDERLINE_TEXT_FLAG
            if (!AccountBean.firstGoApp && AccountBean.userId.isBlank()) {
                AccountBean.userId = equipmentId() ?: "00000001"
            }

            //第二次进入 直接走逻辑
            if (AccountBean.firstGoApp) {
                //初始化ad
                AdUtils.initAd(this.applicationContext)
                starLoadingCount()
                mViewModel.showLoadView.value = true
            } else {
                mViewModel.showLoadView.value = false
            }
        } else {
            //加载不成功
            var loadingNotSuccess = true
            //当前进度值
            var loadingValue = 0
            mViewModel.showLoadView.value = true
            //显示loading
            //结束后隐藏
            mViewModel.launchUI {
                var loading = true
                while (loading) {
                    if (loadingValue >= mViewModel.showTotalCount.value!!) {
                        loading = false
                        mViewModel.showLoadView.value = false
                        if (loadingNotSuccess) {
                            finish()
                        } else {
                            starHomeAc()
                        }
                    } else {
                        delay(10)
                        mViewModel.showLoadingCount.value = loadingValue++
                    }
                }
            }

            mViewModel.viewModelScope.async {
                var loadingFail = true
                while (loadingFail) {
                    if (isAppOpenAdAvailable()) {
                        loadingFail = false
                        loadingValue = mViewModel.showTotalCount.value!!
                        loadingNotSuccess = false
                    } else {
                        AdUtils.loadAppOpenAd()
                    }
                }
            }
        }
    }

    override fun loadData(isRefresh: Boolean) {

    }

    override fun onClick(v: View?) {
        super.onClick(v)
        if (v != null) {
            AppTool.singleClick(v) {
                when (v.id) {
                    R.id.privacy_policy_tv -> {
                        val intent = Intent(this, H5Activity::class.java).apply {
                            putExtra("h5url", UrlPath.PRIVACY_POLICY)
                        }
                        startActivity(intent)
                    }
                    R.id.user_agreement_tv -> {
                        val intent = Intent(this, H5Activity::class.java).apply {
                            putExtra("h5url", UrlPath.USER_AGREEMENT)
                        }
                        startActivity(intent)
                    }
                    R.id.start_btn -> {
                        //同意隐私政策与权限
                        val visibility = bindingView.startBtn.visibility
                        if (checkState && visibility == View.VISIBLE) {
                            //初始化ad
                            AdUtils.initAd(this.applicationContext)
                            mViewModel.showLoadView.value = true
                            starLoadingCount()
                            AccountBean.firstGoApp = true
                        } else {
                            resources.getString(R.string.please_agree_policy).showToast()
                        }
                    }
                    R.id.check_state_iv -> {
                        checkState = !checkState
                        mViewModel.checkState.value = checkState
                    }
                }
            }
        }
    }

    /**
     * 开始计时loading
     */
    private fun starLoadingCount() {
        var loading = true
        var loadingValue = 0

        fun startNext() {
            loading = false
            checkRecordAudio()
        }

        mViewModel.launchUI {
            while (loading) {
                if (loadingValue >= mViewModel.showTotalCount.value!!) {
                    //计数器 >= 超时时间
                    //认为获取ad得不到直接跳转home页面
                    //如果超时时间内,有ad对象则停止循环直接走
                    if (mViewModel.showLoadingCount.value ?: 0 >= mViewModel.adLoadTimeOutTime.value ?: 0) {
                        startNext()
                    } else {
                        if (AdUtils.openScreenAd.value == null) {
                            delay(10)
                            mViewModel.showLoadingCount.value = loadingValue++
                        } else {
                            startNext()
                        }
                    }
                } else {
                    delay(10)
                    mViewModel.showLoadingCount.value = loadingValue++
                }
            }
        }
    }

    /**
     * 跳转主页
     */
    private fun starHomeAc() {
        startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
        //当前是冷启动
        if (!checkInit) {
            //通过eventBus发送事件在home显示ad
            LiveEventBus.get(LiveEventData.HOME_SHOW_INSERT_AD, Boolean::class.java)
                .postDelay(true, 100)
        } else {
            //当前是热启动
            //通过eventBus发送事件在home显示ad
            LiveEventBus.get(LiveEventData.HOME_SHOW_APP_OPEN_AD, Boolean::class.java)
                .postDelay(true, 100)
        }
        finish()
    }


    /**
     * 检查是否允许了录音权限
     */
    private fun checkRecordAudio() {
        if (XXPermissions.isGranted(
                this,
                Manifest.permission.RECORD_AUDIO
            )
        ) {
            starHomeAc()
        } else {
            XXPermissions.with(this)
                .permission(Manifest.permission.RECORD_AUDIO)
                .request(object : OnPermissionCallback {
                    override fun onGranted(
                        permissions: MutableList<String>?,
                        all: Boolean
                    ) {
                        if (all) {
                            starHomeAc()
                        }
                    }

                    override fun onDenied(
                        permissions: MutableList<String>?,
                        never: Boolean
                    ) {
                        starHomeAc()
                    }
                })
        }
    }
}