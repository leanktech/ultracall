package com.ultralab.ultracall.activity.country

import androidx.databinding.ObservableArrayList
import com.alibaba.fastjson.JSON
import com.ultralab.base_lib.base.BaseViewMode
import com.ultralab.base_lib.tool.*
import com.ultralab.ultracall.activity.bean.CountryBean
import com.ultralab.ultracall.local.AccountBean
import org.apache.commons.text.StringEscapeUtils


/**
 * @ClassName: SelectCountryViewModel
 * @Description: 类作用描述
 * @Author: jason@leanktech.com
 * @Date: 2021/9/8 16:25
 */
class SelectCountryViewModel : BaseViewMode() {

    /**
     * 悬浮头部固定数据
     */
    val topData = ObservableArrayList<String>()

    /**
     * 存格式化后的 全球城市区域代号
     */
    val data = ObservableArrayList<CountryBean>()

    fun loadCountryData(callback: (Boolean) -> Unit) {
        val assetsJson = getAssetsJson("country_list", ActivityMgr.getContext())
        assetsJson.toJson()?.let { json ->
            var unescapeJson = StringEscapeUtils.unescapeJson(json)
            unescapeJson = unescapeJson.replace("\t", "")
            //字符串截取 1-length
            unescapeJson = unescapeJson.substring(1, unescapeJson.length - 1)
            val jsonObject = JSON.parseObject(unescapeJson)
            val all = jsonObject.getJSONArray("all")
            //使用新集合承载list
            val newList = mutableListOf<CountryBean>()

            for (i in all.indices){
                val newBean = CountryBean(all[i].toMap())
                newList.add(i,newBean)
            }
            newList.sortBy { it.cname.getSortKey() }

            newList.forEach {
                //获取开头
                val sortKey = it.cname.getSortKey()
                if (sortKey.isNotEmpty()) {
                    topData.add(sortKey)
                }
            }
            if (data.isNotEmpty()) {
                data.clear()
            }
            //添加标题头
            topData.add(0,"Recommended")
            topData.add(1,"Recommended")
//            topData.add(2,"Recommended")
            //添加额外显示 国家
            newList.add(0,CountryBean(hashMapOf("cname" to " India","cflag" to "IN.png","ccode" to "91")))
            newList.add(1,CountryBean(hashMapOf("cname" to " United States","cflag" to "US.png","ccode" to "1")))
            newList.forEach {
                //找到上一次点击存过的item，对本次进行变色处理
                if (AccountBean.lastSelectedCountry.isNotNullEmpty()){
                    val split = AccountBean.lastSelectedCountry.split(",")
                    if (split.size > 2){
                        val name = split[1]
                        if (name == it.cname){
                            it.checkItem = true
                        }
                    }
                }
            }
            data.addAll(newList)
            callback.invoke(true)
        }
    }

    /**
     * 设置选择item 变色
     */
    fun selectItem(bean: CountryBean){
        data.forEach {
            it.checkItem = false
        }
        bean.checkItem = true
    }

}