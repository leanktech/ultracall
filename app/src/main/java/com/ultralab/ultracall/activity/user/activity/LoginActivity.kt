package com.ultralab.ultracall.activity.user.activity

import android.annotation.SuppressLint
import android.graphics.Paint
import android.view.View
import com.gyf.immersionbar.ImmersionBar
import com.ultralab.base_lib.activity.BaseActivity
import com.ultralab.base_lib.tool.AppTool
import com.ultralab.base_lib.tool.StatusBarTool
import com.ultralab.base_lib.tool.showToast
import com.ultralab.ultracall.R
import com.ultralab.ultracall.activity.user.viewmodel.LoginViewModel
import com.ultralab.ultracall.databinding.LoginActivityBinding
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @ClassName: LoginActivity
 * @Description: 登录页面
 * @Author: jason@leanktech.com
 * @Date: 2021/9/2 10:54
 */
class LoginActivity : BaseActivity<LoginActivityBinding>() {

    private val mViewMode by viewModel<LoginViewModel>()

    override fun getLayoutId(): Int = R.layout.login_activity

    override val showToolBar: Boolean
        get() = false

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun initView() {
        //设置状态栏高度
        bindingView.statusBar.layoutParams.height =
            StatusBarTool.getStatusBarHeight(bindingView.statusBar.context)
        ImmersionBar.with(this).statusBarView(bindingView.statusBar).init()
        bindingView.vm = mViewMode
        bindingView.lifecycleOwner = this
        bindingView.presenter = this


        //下划线  抗锯齿
        bindingView.alreadyLoginTv.paint.flags = Paint.UNDERLINE_TEXT_FLAG
        bindingView.alreadyLoginTv.paint.isAntiAlias = true

    }

    override fun loadData(isRefresh: Boolean) {

    }

    override fun onClick(v: View?) {
        super.onClick(v)
        if (v != null) {
            AppTool.singleClick(v) {
                when (v.id) {
                    R.id.close_iv -> {
                        finish()
                    }
                    R.id.root_login_layout -> {
                        AppTool.closeKeyboard(this)
                    }
                    R.id.sign_in_btn -> {
                        "Temporarily open".showToast()
                    }
                    R.id.close_in_btn -> {
                        finish()
                    }
                    R.id.already_login_tv -> {
                        "Temporarily open".showToast()
                    }
                }
            }
        }
    }
}