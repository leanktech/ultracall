package com.ultralab.ultracall.activity.user.viewmodel

import androidx.lifecycle.MutableLiveData
import com.ultralab.base_lib.base.BaseViewMode
import com.ultralab.base_lib.tool.init

/**
 * @ClassName: SettingViewModel
 * @Description: 类作用描述
 * @Author: jason@leanktech.com
 * @Date: 2021/9/8 16:12
 */
class SettingViewModel : BaseViewMode() {
    /**
     * 显示当前版本信息
     */
    val versionInfo = MutableLiveData<String>().init("dev 1.0.0")
}