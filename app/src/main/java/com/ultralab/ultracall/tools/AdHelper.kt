package com.ultralab.ultracall.tools

import android.app.Activity
import com.google.android.gms.ads.AdError
import com.google.android.gms.ads.FullScreenContentCallback
import com.ultralab.base_lib.tool.ActivityMgr
import com.ultralab.base_lib.tool.print
import java.util.*


/**
 * @Date :2021/10/29
 * @Description: 展示广告逻辑页面
 * @Author: jason@leanktech.com
 */
//回调
private var callBacks: AdCallBack? = null

/**
 * 展示插页广告内容
 * @param activity
 * @param callBack
 * @return show ad
 */
fun showInsertAd(activity: Activity, callBack: AdCallBack.Builder.() -> Unit) {
    //赋值
    callBack.apply {
        callBacks = AdCallBack().apply { setCallBack(callBack) }
    }
    //ad
    val insertAd = AdUtils.openScreenAd.value
    if (insertAd != null) {
        insertAd.fullScreenContentCallback = object : FullScreenContentCallback() {
            override fun onAdDismissedFullScreenContent() {
                super.onAdDismissedFullScreenContent()
                AnalyticsUtils.closeClodStart()
                callBacks?.build?.adDisMiss?.invoke(true)
                AdUtils.openScreenAd.value = null
                AdUtils.appHaveAdIsShow = false
            }

            override fun onAdFailedToShowFullScreenContent(p0: AdError) {
                super.onAdFailedToShowFullScreenContent(p0)
                callBacks?.build?.adIsNull?.invoke(true)
                AdUtils.openScreenAd.value = null
            }
        }
        //
        callBacks?.build?.showAdView?.invoke(true)
        AnalyticsUtils.openClodStart()
        AdUtils.appHaveAdIsShow = true
        insertAd.show(activity)
    } else {
        callBacks?.build?.adIsNull?.invoke(true)
    }
}

/**
 * 展示签到广告
 * @param activity
 * @param callBack
 * @return show ad
 */
fun showSignInAd(activity: Activity, callBack: AdCallBack.Builder.() -> Unit) {
    //赋值
    callBack.apply {
        callBacks = AdCallBack().apply { setCallBack(callBack) }
    }
    val signInAd = AdUtils.signInAd.value
    if (signInAd != null) {
        signInAd.fullScreenContentCallback = object : FullScreenContentCallback() {
            override fun onAdDismissedFullScreenContent() {
                super.onAdDismissedFullScreenContent()
                AnalyticsUtils.closeCheckIn()
                AdUtils.signInAd.value = null
                AdUtils.appHaveAdIsShow = false
                AdUtils.loadSignInAd(ActivityMgr.getContext())
                callBacks?.build?.adDisMiss?.invoke(true)

            }

            override fun onAdFailedToShowFullScreenContent(p0: AdError) {
                super.onAdFailedToShowFullScreenContent(p0)
                callBacks?.build?.adIsNull?.invoke(true)
                AdUtils.signInAd.value = null
            }
        }
        //
        callBacks?.build?.showAdView?.invoke(true)
        AnalyticsUtils.openCheckIn()
        AdUtils.appHaveAdIsShow = true
        signInAd.show(activity)
    } else {
        callBacks?.build?.adIsNull?.invoke(true)
    }
}

/**
 * 检查广告是否在 n 小时前加载的实用方法。
 * @param numHours 小时
 * */
private fun wasLoadTimeLessThanNHoursAgo(numHours: Long): Boolean {
    val dateDifference = Date().time - AdUtils.loadTime
    val numMilliSecondsPerHour: Long = 3600000
    return dateDifference < numMilliSecondsPerHour * numHours
}

/**
 * 检查广告是否存在并且可以显示的实用方法。
 * */
fun isAppOpenAdAvailable(): Boolean {
    return AdUtils.appOpenAd.value != null && wasLoadTimeLessThanNHoursAgo(4)
}

/**
 * 展示app open 广告
 * @param activity
 * @param callBack
 * @return show ad
 */
fun showAppOpenAd(activity: Activity, callBack: AdCallBack.Builder.() -> Unit) {
    //赋值
    callBack.apply {
        callBacks = AdCallBack().apply { setCallBack(callBack) }
    }
    // 仅在当前没有打开应用的广告时才显示广告
    // 并且有广告。
    if (!AdUtils.isShowingAd && isAppOpenAdAvailable()) {
        val fullScreenContentCallback: FullScreenContentCallback =
            object : FullScreenContentCallback() {
                override fun onAdDismissedFullScreenContent() {
                    //将引用设置为 null，以便 isAdAvailable() 返回 false。
                    AdUtils.appOpenAd.value = null
                    AdUtils.isShowingAd = false
                    AdUtils.appHaveAdIsShow = false
                    //重新加载app open 广告
                    AdUtils.loadAppOpenAd()
                    AnalyticsUtils.closeHotStart()
                    callBacks?.build?.adDisMiss?.invoke(true)
                }

                override fun onAdFailedToShowFullScreenContent(adError: AdError) {
                    callBacks?.build?.adIsNull?.invoke(true)
                }

                override fun onAdShowedFullScreenContent() {
                    AdUtils.isShowingAd = true
                    AdUtils.appHaveAdIsShow = true
                    callBacks?.build?.showAdView?.invoke(true)
                }
            }
        AdUtils.appOpenAd.value?.let {
            it.fullScreenContentCallback = fullScreenContentCallback
            AnalyticsUtils.openHotStart()
            it.show(activity)
        }
    } else {
        AdUtils.appOpenAd.value = null
        callBacks?.build?.adIsNull?.invoke(true)
    }
}

/**
 * 展示激励视频
 * @param activity
 * @param callBack
 * @return show ad
 */
fun showRewardedAd(activity: Activity, callBack: AdCallBack.Builder.() -> Unit) {
    //赋值
    callBack.apply {
        callBacks = AdCallBack().apply { setCallBack(callBack) }
    }
    if (AdUtils.rewardedVideo.value != null) {
        val rewardedAd = AdUtils.rewardedVideo.value
        rewardedAd?.fullScreenContentCallback = object : FullScreenContentCallback() {
            override fun onAdDismissedFullScreenContent() {
                super.onAdDismissedFullScreenContent()
                AdUtils.rewardedVideo.value = null
                AdUtils.appHaveAdIsShow = false
                AdUtils.loadRewardedAd(ActivityMgr.getContext().applicationContext)
                callBacks?.build?.adDisMiss?.invoke(true)
            }

            override fun onAdFailedToShowFullScreenContent(p0: AdError) {
                super.onAdFailedToShowFullScreenContent(p0)
                callBacks?.build?.adIsNull?.invoke(true)
            }

            override fun onAdShowedFullScreenContent() {
                super.onAdShowedFullScreenContent()
                AdUtils.appHaveAdIsShow = true
                callBacks?.build?.showAdView?.invoke(true)
            }
        }
        rewardedAd?.show(activity) {
            "看完结束返回info type=${it.type} amount=${it.amount}".print("RewardedAd")
            callBacks?.build?.rewardItem?.invoke(it)
        }
    } else {
        AdUtils.rewardedVideo.value = null
        callBacks?.build?.adIsNull?.invoke(true)
    }
}


