package com.ultralab.ultracall.tools

import android.app.Activity
import android.app.Application
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import com.ultralab.base_lib.tool.ActivityMgr
import com.ultralab.base_lib.tool.print
import com.ultralab.ultracall.CallApplication
import com.ultralab.ultracall.activity.ad_activity.NativeAdActivity
import com.ultralab.ultracall.activity.home.activity.SplashActivity


/**
 * @Description: 实现Application ActivityLifecycleCallback接口
 * 监听app前后台
 * @Author: even@leanktech.com
 * @Date: 2021/10/11 18:07
 */
object AppManager : LifecycleObserver,
    Application.ActivityLifecycleCallbacks {

    /** log tag */
    private const val APP_OPEN_TAG = "AppOpenAd"

    /** activity 计数器 */
    private var mFinalCount = 0

    /** app 主体 对象*/
    private var myApplication: CallApplication? = null

    /** 临时变量 记录是否已经退到后台了 */
    var isForegroundMethodTemp = false

    fun init(myApplication: CallApplication) {
        this.myApplication = myApplication
        this.myApplication!!.registerActivityLifecycleCallbacks(this)
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
    }

    /**
     * 监听前台事件
     * 并展示app open 广告
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppStart() {
        "进入onStart下,条件是否成立,弹出appOpenAd".print(APP_OPEN_TAG)
        if (isForegroundMethodTemp && isAppOpenAdAvailable()) {
            if (ActivityMgr.getOneActivity(NativeAdActivity::class.java) == null &&
                ActivityMgr.getOneActivity(SplashActivity::class.java) == null) {
                val context = ActivityMgr.getContext()
                context.startActivity(Intent(context, SplashActivity::class.java).apply {
                    putExtra("checkInit", true)
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK
                })
                isForegroundMethodTemp = false
            }
        }
    }

    /**
     * 实现ActivityLifecycleCallbacks
     */
    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
    }

    override fun onActivityStarted(activity: Activity) {
        mFinalCount++
        //如果mFinalCount ==1，说明是从后台到前台
        if (mFinalCount == 1) {
            CallApplication.isColdLaunch = false
        }
    }

    override fun onActivityStopped(activity: Activity) {
        mFinalCount--
        //如果mFinalCount ==0，说明是前台到后台
        if (mFinalCount == 0) {
            isForegroundMethodTemp = true
        }
    }

    override fun onActivityResumed(activity: Activity) {
    }

    override fun onActivityPaused(activity: Activity) {
    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
    }

    override fun onActivityDestroyed(activity: Activity) {
    }

}