package com.ultralab.ultracall.tools

import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import com.ultralab.base_lib.tool.ActivityMgr


/**
 * @Date :2021/10/25
 * @Description: 埋点统计
 * @Author: jason@leanktech.com
 */
object AnalyticsUtils {

    /**
     * 声明分析对象
     */
    private val mFirebaseAnalytics by lazy {
        FirebaseAnalytics.getInstance(ActivityMgr.getContext())
    }

    /**
     * 冷启动ad
     */
    fun openClodStart() {
        val bundle = Bundle().apply {
            putString("Open_int_Cold_start", "打开冷启ad")
        }
        mFirebaseAnalytics.logEvent("Open_int_Cold_start", bundle)
    }

    /**
     * 冷启动关闭ad
     */
    fun closeClodStart() {
        val bundle = Bundle().apply {
            putString("Close_int_Cold_start", "关闭冷启ad")
        }
        mFirebaseAnalytics.logEvent("Close_int_Cold_start", bundle)
    }

    /**
     * 热启动 广告
     */
    fun openHotStart() {
        val bundle = Bundle().apply {
            putString("Open_int_Hot_start", "打开热启ad")
        }
        mFirebaseAnalytics.logEvent("Open_int_Hot_start", bundle)
    }

    /**
     * 热启动 关闭广告
     */
    fun closeHotStart() {
        val bundle = Bundle().apply {
            putString("Close_int_Hot_start", "离开热启ad")
        }
        mFirebaseAnalytics.logEvent("Close_int_Hot_start", bundle)
    }

    /**
     * 签到 ad
     */
    fun openCheckIn() {
        val bundle = Bundle().apply {
            putString("Open_int_check_in", "打开签到ad")
        }
        mFirebaseAnalytics.logEvent("Open_int_check_in", bundle)
    }

    /**
     * 签到 关闭 ad
     */
    fun closeCheckIn() {
        val bundle = Bundle().apply {
            putString("Close_int_check_in", "关闭签到ad")
        }
        mFirebaseAnalytics.logEvent("Close_int_check_in", bundle)
    }

    /**
     * native ad
     */
    fun openNativeAd() {
        val bundle = Bundle().apply {
            putString("Open_nat_check_in", "打开签到原生ad")
        }
        mFirebaseAnalytics.logEvent("Open_nat_check_in", bundle)
    }

    /**
     * native click ad
     */
    fun clickNativeAd() {
        val bundle = Bundle().apply {
            putString("Click_button_check_in", "点击签到按钮")
        }
        mFirebaseAnalytics.logEvent("Click_button_check_in", bundle)
    }


}