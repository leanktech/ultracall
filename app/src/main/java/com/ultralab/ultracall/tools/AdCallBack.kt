package com.ultralab.ultracall.tools

import com.google.android.gms.ads.rewarded.RewardItem

/**
 * @ClassName: CallNumberBack
 * @Description: call number dsl call back
 * @Author: jason@leanktech.com
 * @Date: 2021/8/24 14:34
 */
class AdCallBack {

    lateinit var build: Builder

    fun setCallBack(call: Builder.() -> Unit) {
        this.build = Builder().also(call)
    }

    inner class Builder {
        //关闭
        internal var adDisMiss: ((Boolean) -> Unit)? = null

        //ad show
        internal var showAdView: ((Boolean) -> Unit)? = null

        //ad is null
        internal var adIsNull: ((Boolean) -> Unit)? = null

        //返回激励广告奖励对象
        internal var rewardItem: ((RewardItem) -> Unit)? = null

        fun adDisMiss(action: (Boolean) -> Unit) {
            adDisMiss = action
        }

        fun showAdView(action: (Boolean) -> Unit) {
            showAdView = action
        }

        fun adIsNull(action: (Boolean) -> Unit) {
            adIsNull = action
        }

        fun rewardItem(action: (RewardItem) -> Unit) {
            rewardItem = action
        }
    }
}