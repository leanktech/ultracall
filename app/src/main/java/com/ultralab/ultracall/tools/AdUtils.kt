package com.ultralab.ultracall.tools

import android.content.Context
import androidx.annotation.NonNull
import androidx.databinding.BaseObservable
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.appopen.AppOpenAd
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.rewarded.RewardedAd
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback
import com.ultralab.base_lib.tool.ActivityMgr
import com.ultralab.base_lib.tool.init
import com.ultralab.base_lib.tool.print
import com.ultralab.ultracall.GoogleAd
import java.util.*

/**
 * @Description: ad 工具类
 * @Author: jason@leanktech.com
 * @Date: 2021/10/11 13:16
 */
object AdUtils : BaseObservable() {
    /**
     * log tag
     */
    private const val AD_INSERT_TAG = "adInsert"
    private const val AD_SIGN_IN_TAG = "adSignIn"
    private const val AD_NATIVE_TAG = "adNative"
    private const val AD_REWARDED_TAG = "adRewarded"
    private const val AD_APP_OPEN_TAG = "adAppOpen"

    /**
     * 开屏广告
     */
    val openScreenAd = MutableLiveData<InterstitialAd?>().init(null)

    /**
     * 签到广告
     */
    val signInAd = MutableLiveData<InterstitialAd?>().init(null)

    /**
     * 原生广告
     */
    val primitiveAd = MutableLiveData<NativeAd?>().init(null)

    /**
     * 激励视频
     */
    val rewardedVideo = MutableLiveData<RewardedAd?>().init(null)

    /**
     * app open ad
     */
    val appOpenAd = MutableLiveData<AppOpenAd?>().init(null)

    /**
     * 当前是否有广告在显示
     * 有：拦截不显示广告
     * 否：不拦截显示广告
     */
    var appHaveAdIsShow = false

    /** app  open ad 声明 ↓↓↓↓ */
    private var loadCallback: AppOpenAd.AppOpenAdLoadCallback? = null

    /** 过期时间 */
    var loadTime = 0L

    var isShowingAd = false

    fun initAd(context: Context) {
        "开始load ad ".print()
        loadSplashAd(context)
        loadSignInAd(context)
        loadSignInNativeAd(context)
        loadRewardedAd(context)
        loadAppOpenAd()
    }

    /**
     * 先初始化 splash ad 资源
     */
    fun loadSplashAd(context: Context) {
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(
            context, GoogleAd.GOOGLE_AD_SPLASH_ID, adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdLoaded(@NonNull interstitialAd: InterstitialAd) {
                    "Splash onAdLoaded success".print(AD_INSERT_TAG)
                    openScreenAd.value = interstitialAd
                }

                override fun onAdFailedToLoad(@NonNull loadAdError: LoadAdError) {
                    "Splash onAdFailedToLoad error${loadAdError.message}".print(AD_INSERT_TAG)
                    openScreenAd.value = null
                }
            })
    }

    /**
     * 初始化 sign in ad 资源
     */
    fun loadSignInAd(context: Context) {
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(
            context, GoogleAd.GOOGLE_AD_CHECK_IN_ID, adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdLoaded(@NonNull interstitialAd: InterstitialAd) {
                    "sign in onAdLoaded success".print(AD_SIGN_IN_TAG)
                    signInAd.value = interstitialAd
                }

                override fun onAdFailedToLoad(@NonNull loadAdError: LoadAdError) {
                    "sign in onAdFailedToLoad error${loadAdError.message}".print(AD_SIGN_IN_TAG)
                    if (loadAdError.code == GoogleAd.ERROR_NO_AD_CONFIG) {
                        //todo 没有广告返回
                    }
                    signInAd.value = null
                }
            })
    }

    /**
     * 初始化签到native ad
     */
    fun loadSignInNativeAd(context: Context) {
        val adLoader = AdLoader.Builder(context, GoogleAd.GOOGLE_AD_NATIVE_ID)
            .forNativeAd { nativeAd ->
                "sign in native onAdLoaded success".print(AD_NATIVE_TAG)
                primitiveAd.value = nativeAd
            }
            .withAdListener(object : AdListener() {
                override fun onAdFailedToLoad(error: LoadAdError) {
                    if (error.code == GoogleAd.ERROR_NO_AD_CONFIG) {
                        //todo 没有广告返回
                    }
                    primitiveAd.value = null
                    "sign in native Failed to native ad: $error".print(AD_NATIVE_TAG)
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                }

            }).build()
        adLoader.loadAd(AdRequest.Builder().build())
    }

    /**
     * 加载激励视频
     */
    fun loadRewardedAd(context: Context) {
        RewardedAd.load(
            context,
            GoogleAd.GOOGLE_AD_REWARDED_ID,
            AdRequest.Builder().build(),
            object : RewardedAdLoadCallback() {
                override fun onAdLoaded(p0: RewardedAd) {
                    super.onAdLoaded(p0)
                    "reward ad load reward success".print(AD_REWARDED_TAG)
                    rewardedVideo.value = p0
                }

                override fun onAdFailedToLoad(error: LoadAdError) {
                    super.onAdFailedToLoad(error)
                    "reward ad load reward error code=${error.code}  error message=${error.message}".print(
                        AD_REWARDED_TAG
                    )
                    if (error.code == GoogleAd.ERROR_NO_AD_CONFIG) {
                        //todo 没有广告返回
                    }
                    rewardedVideo.value = null
                }
            })
    }

    /**
     * 初始化app open ad
     */
    fun loadAppOpenAd() {
        if (isAppOpenAdAvailable()) {
            return
        }
        "初始化app open ad".print(AD_APP_OPEN_TAG)
        loadCallback = object : AppOpenAd.AppOpenAdLoadCallback() {
            override fun onAdLoaded(p0: AppOpenAd) {
                super.onAdLoaded(p0)
                "onAdLoaded ".print(AD_APP_OPEN_TAG)
                appOpenAd.value = p0
                loadTime = Date().time
            }

            override fun onAdFailedToLoad(error: LoadAdError) {
                super.onAdFailedToLoad(error)
                if (error.code == GoogleAd.ERROR_NO_AD_CONFIG) {
                    //todo 没有广告返回
                }
                "onAdFailedToLoad errorMessage=${error.message} errorCode=${error.code}".print(
                    AD_APP_OPEN_TAG
                )
            }
        }

        if (loadCallback != null) {
            val request = AdRequest.Builder().build()
            AppOpenAd.load(
                ActivityMgr.getContext().applicationContext,
                GoogleAd.GOOGLE_AD_APP_OPEN_ID,
                request,
                AppOpenAd.APP_OPEN_AD_ORIENTATION_PORTRAIT,
                loadCallback!!
            )
        }
    }
}