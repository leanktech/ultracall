package com.ultralab.ultracall.tools

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.telephony.TelephonyManager
import com.ultralab.base_lib.tool.ActivityMgr
import com.ultralab.base_lib.tool.printError
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.RandomAccessFile
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern


/**
 * @ClassName: DeviceTool
 * @Description: 获取本机信息
 * @Author: jason@leanktech.com
 * @Date: 2021/9/3 16:19
 */

private var sID: String? = null
private const val INSTALLATION = "INSTALLATION"

/**
 * 获取设备唯一标识
 * @param context 上下文对象，默认为ActivityMgr.getContext()
 * @return IMEI，获取不到返回空字符串
 */
@Synchronized
fun equipmentId(context: Context = ActivityMgr.getContext()): String? {
    if (sID == null) {
        val installation = File(context.filesDir, INSTALLATION)
        sID = try {
            if (!installation.exists()) writeInstallationFile(installation)
            readInstallationFile(installation)
        } catch (e: java.lang.Exception) {
            throw RuntimeException(e)
        }
    }
    "sid = ${getStringToNumber(sID ?: "")}".printError("SID")
    return sID?.let { getStringToNumber(it) }
}

@Throws(IOException::class)
private fun readInstallationFile(installation: File): String? {
    val f = RandomAccessFile(installation, "r")
    val bytes = ByteArray((f.length() as Long).toInt())
    f.readFully(bytes)
    f.close()
    return String(bytes)
}

@Throws(IOException::class)
private fun writeInstallationFile(installation: File) {
    val out = FileOutputStream(installation)
    val id = UUID.randomUUID().toString()
    out.write(id.toByteArray())
    out.close()
}

/**
 * 字符串中获取数字
 */
private fun getStringToNumber(string: String): String {
    val regEx = "[^0-9]"
    val p: Pattern = Pattern.compile(regEx)
    val m: Matcher = p.matcher(string)
    val strNumber = m.replaceAll("").trim()
    return if (strNumber.length > 8) {
        strNumber.substring(0, 8)
    } else {
        strNumber
    }
}

/**
 * TelephonyManager（从SIM或CDMA设备）获取国家代码，如果不可用，请尝试从本地配置获取国家代码。
 */
fun getDeviceCountryCode(context: Context): String {
    var countryCode: String?
    // try to get country code from TelephonyManager service
    val tm = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
    // query first getSimCountryIso()
    countryCode = tm.simCountryIso
    if (countryCode != null && countryCode.length == 2) return countryCode.toLowerCase(Locale.ROOT)
    countryCode = if (tm.phoneType == TelephonyManager.PHONE_TYPE_CDMA) {
        // special case for CDMA Devices
        getCDMACountryIso()
    } else {
        // for 3G devices (with SIM) query getNetworkCountryIso()
        tm.networkCountryIso
    }
    if (countryCode != null && countryCode.length == 2) return countryCode.toLowerCase(Locale.ROOT)

    // if network country not available (tablets maybe), get country code from Locale class
    countryCode = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        context.resources.configuration.locales[0].country
    } else {
        context.resources.configuration.locale.country
    }
    return if (countryCode != null && countryCode.length == 2) countryCode.toLowerCase(Locale.ROOT) else "us"
}

@SuppressLint("PrivateApi")
private fun getCDMACountryIso(): String? {
    try {
        // try to get country code from SystemProperties private class
        val systemProperties = Class.forName("android.os.SystemProperties")
        val get: Method = systemProperties.getMethod("get", String::class.java)

        // get homeOperator that contain MCC + MNC
        val homeOperator = get.invoke(
            systemProperties,
            "ro.cdma.home.operator.numeric"
        ) as String
        if (homeOperator.isNotEmpty()){
            // first 3 chars (MCC) from homeOperator represents the country code
            when (homeOperator.substring(0, 3).toInt()) {
                330 -> return "PR"
                310 -> return "US"
                311 -> return "US"
                312 -> return "US"
                316 -> return "US"
                283 -> return "AM"
                460 -> return "CN"
                455 -> return "MO"
                414 -> return "MM"
                619 -> return "SL"
                450 -> return "KR"
                634 -> return "SD"
                434 -> return "UZ"
                232 -> return "AT"
                204 -> return "NL"
                262 -> return "DE"
                247 -> return "LV"
                255 -> return "UA"
            }
        }else{
            return null
        }

    } catch (ignored: ClassNotFoundException) {
    } catch (ignored: NoSuchMethodException) {
    } catch (ignored: IllegalAccessException) {
    } catch (ignored: InvocationTargetException) {
    } catch (ignored: NullPointerException) {
    }
    return null
}


/**
 * 获取手机厂商+获取手机型号
 *
 * @return 手机厂商+手机型号
 */
fun getSystemModelDeviceBrand(): String {
    return if (Build.BRAND != null) "${Build.BRAND}-${Build.MODEL}" else Build.MODEL
}