package com.ultralab.ultracall.server

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.ultralab.base_lib.tool.ActivityMgr
import com.ultralab.base_lib.tool.print
import com.ultralab.ultracall.conversation.SoundPoolManager

/**
 * @ClassName: HeadsetReceiver
 * @Description: 监听耳机
 * @Author: jason@leanktech.com
 * @Date: 2021/9/3 13:45
 */
class HeadsetReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        intent?.let {
            when (intent.action) {
                Intent.ACTION_HEADSET_PLUG -> {
                    //addHint(Intent.ACTION_HEADSET_PLUG)
                    val state = intent.getIntExtra("state", 0)
                    if (state == 1) {
                        //耳机已插入
                        "耳机已插入手机".print()
                        SoundPoolManager.getInstance(ActivityMgr.getContext())
                            ?.setOpenSpeaker(false)
                    } else {
                        //耳机已拔出
                    }
                }
                else -> {
                }
            }
        }
    }
}