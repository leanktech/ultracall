package com.ultralab.ultracall.conversation

import android.content.Context
import android.media.AudioManager
import android.media.SoundPool
import android.os.Build
import com.ultralab.base_lib.tool.print
import com.ultralab.ultracall.R


/**
 * @ClassName: SoundPoolManager
 * @Description: 接听 挂断 异常 提示音
 * @Author: jason@leanktech.com
 * @Date: 2021/8/24 13:58
 */
class SoundPoolManager private constructor(context: Context) {
    private var playing = false
    private var loaded = false
    private var playingCalled = false
    private var volume: Float = 0F
    private var soundPool: SoundPool? = null
    private val ringingSoundId: Int
    private var ringingStreamId = 0
    private val disconnectSoundId: Int
    private var audioManager: AudioManager? = null

    fun playRinging() {
        if (loaded && !playing) {
            ringingStreamId = soundPool!!.play(ringingSoundId, volume, volume, 1, -1, 1f)
            playing = true
        } else {
            playingCalled = true
        }
    }

    fun stopRinging() {
        if (playing) {
            soundPool!!.stop(ringingStreamId)
            playing = false
        }
    }

    fun playDisconnect() {
        if (loaded && !playing) {
            soundPool!!.play(disconnectSoundId, volume, volume, 1, 0, 1f)
            playing = false
        }
    }

    fun release() {
        if (soundPool != null) {
            soundPool!!.unload(ringingSoundId)
            soundPool!!.unload(disconnectSoundId)
            soundPool!!.release()
            soundPool = null
        }
        instance = null
    }

    /**
     * 切换扬声器and话筒
     * @param isSpeak = true 扬声器 = false 听筒
     */
    fun setOpenSpeaker(isSpeak: Boolean): Boolean {
        "isSpeak= $isSpeak audioManager=$audioManager".print()
        return if (isSpeak) {
            openSpeaker()
            false
        } else {
            closeSpeaker()
            true
        }
    }

    private fun openSpeaker() {
        audioManager?.let {
            it.isSpeakerphoneOn = true
        }
    }

    private fun closeSpeaker() {
        audioManager?.let {
            it.isSpeakerphoneOn = false
        }
    }

    /**
     * 增加音量 减小音量
     * @param upVolume
     */
    fun setVolume(upVolume: Boolean) {
        fun raiseVolume() {
            val currentVolume = audioManager!!.getStreamVolume(AudioManager.STREAM_MUSIC)
            if (currentVolume < audioManager!!.getStreamMaxVolume(AudioManager.STREAM_MUSIC)) {
                audioManager!!.adjustStreamVolume(
                    AudioManager.STREAM_MUSIC,
                    AudioManager.ADJUST_RAISE, AudioManager.FX_FOCUS_NAVIGATION_UP
                )
            }
        }

        fun lowerVolume() {
            val currentVolume = audioManager!!.getStreamVolume(AudioManager.STREAM_MUSIC)
            if (currentVolume > 0) {
                audioManager!!.adjustStreamVolume(
                    AudioManager.STREAM_MUSIC,
                    AudioManager.ADJUST_LOWER, AudioManager.FX_FOCUS_NAVIGATION_UP
                )
            }
        }

        audioManager?.let {
            if (upVolume) {
                raiseVolume()
            } else {
                lowerVolume()
            }
        }
    }


    companion object {
        private var instance: SoundPoolManager? = null
        fun getInstance(context: Context): SoundPoolManager? {
            if (instance == null) {
                instance = SoundPoolManager(context)
            }
            return instance
        }
    }

    init {
        // AudioManager audio settings for adjusting the volume
        audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        audioManager?.let {
            val actualVolume = it.getStreamVolume(AudioManager.STREAM_MUSIC).toFloat()
            //val maxVolume = it.getStreamMaxVolume(AudioManager.STREAM_MUSIC).toFloat()
            //volume = actualVolume / maxVolume
            volume = actualVolume
        }
        // Load the sounds
        val maxStreams = 1
        soundPool = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            SoundPool.Builder()
                .setMaxStreams(maxStreams)
                .build()
        } else {
            SoundPool(maxStreams, AudioManager.STREAM_MUSIC, 0)
        }
        soundPool!!.setOnLoadCompleteListener { _, _, _ ->
            loaded = true
            if (playingCalled) {
                playRinging()
                playingCalled = false
            }
        }
        ringingSoundId = soundPool!!.load(context, R.raw.incoming, 1)
        disconnectSoundId = soundPool!!.load(context, R.raw.disconnect, 1)
    }
}