package com.ultralab.ultracall.model

import com.ultralab.base_lib.base.BaseRepository
import com.ultralab.base_lib.tool.keyvalue.KeyValueMgr

/**
 * @ClassName: HomeRepository
 * @Description: 首页关联接口
 * @Author: jason@leanktech.com
 * @Date: 2021/9/13 15:12
 */
class HomeRepository(private val service: HomeService, private val kvMgr: KeyValueMgr) :
    BaseRepository() {

    /**
     * 获取twilio token
     */
    suspend fun getTwilioToken(): Any? =
        service.getTwilioToken()
}