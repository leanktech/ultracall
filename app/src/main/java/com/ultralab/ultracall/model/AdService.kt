package com.ultralab.ultracall.model

import retrofit2.Call
import retrofit2.http.GET

interface AdService {

    @GET("cityjson?ie=utf-8")
    fun getNetWorkIp():Call<String>?
}