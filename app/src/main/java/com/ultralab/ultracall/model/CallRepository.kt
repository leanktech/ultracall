package com.ultralab.ultracall.model

import com.ultralab.base_lib.base.BaseRepository
import com.ultralab.base_lib.tool.keyvalue.KeyValueMgr

/**
 * @ClassName: CallRepository
 * @Description: 类作用描述
 * @Author: jason@leanktech.com
 * @Date: 2021/9/2 18:38
 */
class CallRepository(private val service: CallService, private val kvMgr: KeyValueMgr) :
    BaseRepository() {

}