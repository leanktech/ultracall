package com.ultralab.ultracall.model

import com.ultralab.ultracall.NetWorkUrl
import retrofit2.http.GET
import retrofit2.http.HeaderMap

/**
 * @ClassName: CallService
 * @Description: 接口
 * @Author: jason@leanktech.com
 * @Date: 2021/9/2 18:39
 */
interface HomeService {

    /**
     * 获取twilio token
     */
    @GET(NetWorkUrl.BASE_URL + NetWorkUrl.ACCESS_TOKEN)
    suspend fun getTwilioToken(@HeaderMap header: Map<String, String> = hashMapOf("Authorization" to "basic dWx0cmE6MS4wLjEw")): Any?
}