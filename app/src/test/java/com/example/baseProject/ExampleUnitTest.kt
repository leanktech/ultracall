package com.ultralab.ultracall

import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * leanktech local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class leanktechUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}