package com.ultralab.weight_lib

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.TextView

/**
 * @ClassName: Sidebar
 * @Description: 类作用描述
 * @Author: jason@leanktech.com
 * @Date: 2021/9/9 18:22
 */
class SideLetterBar : View {
    private var choose = -1
    private val paint = Paint()
    private var showBg = false
    private var onLetterChangedListener: OnLetterChangedListener? = null
    private var overlay: TextView? = null

    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    )

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context?) : super(context)

    /**
     * 设置悬浮的textview
     *
     * @param overlay
     */
    fun setOverlay(overlay: TextView?) {
        this.overlay = overlay
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (showBg) {
            canvas.drawColor(Color.TRANSPARENT)
        }
        val height = height
        val width = width
        val singleHeight = height / b.size
        for (i in b.indices) {
            paint.textSize = resources.getDimension(R.dimen.font_12sp)
            paint.color = Color.parseColor("#999999")
            paint.isAntiAlias = true
            if (i == choose) {
                paint.color = Color.parseColor("#5c5c5c")
                paint.isFakeBoldText = true //加粗
            }
            val xPos = width / 2 - paint.measureText(b[i]) / 2
            val yPos = (singleHeight * i + singleHeight).toFloat()
            canvas.drawText(b[i], xPos, yPos, paint)
            paint.reset()
        }
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        val action = event.action
        val y = event.y
        val oldChoose = choose
        val listener = onLetterChangedListener
        val c = (y / height * b.size).toInt()
        when (action) {
            MotionEvent.ACTION_DOWN -> {
                showBg = true
                if (oldChoose != c && listener != null) {
                    if (c >= 0 && c < b.size) {
                        listener.onLetterChanged(b[c])
                        choose = c
                        invalidate()
                        if (overlay != null) {
                            overlay!!.visibility = VISIBLE
                            overlay!!.text = b[c]
                        }
                    }
                }
            }
            MotionEvent.ACTION_MOVE -> if (oldChoose != c && listener != null) {
                if (c >= 0 && c < b.size) {
                    listener.onLetterChanged(b[c])
                    choose = c
                    invalidate()
                    if (overlay != null) {
                        overlay!!.visibility = VISIBLE
                        overlay!!.text = b[c]
                    }
                }
            }
            MotionEvent.ACTION_UP -> {
                showBg = false
                choose = -1
                invalidate()
                if (overlay != null) {
                    overlay!!.visibility = GONE
                }
            }
        }
        return true
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return super.onTouchEvent(event)
    }

    fun setOnLetterChangedListener(onLetterChangedListener: OnLetterChangedListener?) {
        this.onLetterChangedListener = onLetterChangedListener
    }

    interface OnLetterChangedListener {
        fun onLetterChanged(letter: String?)
    }

    companion object {
        private val b = arrayOf(
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"
        )
    }
}