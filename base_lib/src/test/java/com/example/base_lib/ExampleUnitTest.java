package com.ultralab.base_lib;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * leanktech local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class leanktechUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }
}