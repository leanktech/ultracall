package com.ultralab.base_lib.base

import com.ultralab.base_lib.tool.network.ResponseThrowable


abstract class BaseRepository {

    /**
     * @param remote 远程数据
     * @param local 本地数据
     * @param save 当网络请求成功后，保存数据等操作
     * @param isUseCache 根据本地数据的属性，决定是否使用缓存，默认使用
     */
    suspend fun <T> cacheNetCall(
        remote: suspend () -> BaseResponse<T>,
        local: suspend () -> T?,
        save: suspend (T) -> Unit,
        isUseCache: (T?) -> Boolean = { true }
    ): T {
        val localData = local.invoke()
        return if (isUseCache(localData) && localData != null) localData
        else {
            remote().let { net ->
                if (net.isSuccess()) net.data().also { save(it) }
                throw ResponseThrowable(net)
            }
        }
    }
}