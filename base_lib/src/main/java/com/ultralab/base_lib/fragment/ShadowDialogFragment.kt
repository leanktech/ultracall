package com.ultralab.base_lib.fragment

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.ultralab.base_lib.R


abstract class ShadowDialogFragment<VB : ViewDataBinding>: BaseDialogFragment<VB>(){
    /**
     * 动画，默认为渐入动画
     */
    override val animStyle :Int?= R.style.dialogNoAnim

    /**
     * 绑定DataBinding
     * @param inflater LayoutInflater
     * @param container 父view
     * @param savedInstanceState Bundle
     * @return 显示View
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        bindingView = DataBindingUtil.inflate(activity!!.layoutInflater, getLayoutId(), null, false)
        if (Build.VERSION.SDK_INT >= 32) {
            dialog?.window!!.setDecorFitsSystemWindows(false)
            dialog?.window!!.insetsController?.show(WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS)
            dialog?.window!!.insetsController?.show(WindowInsetsController.APPEARANCE_LIGHT_NAVIGATION_BARS)
        }else{
            dialog?.window!!.decorView.systemUiVisibility = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
            }else{
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            }
        }
        return bindingView.root
    }

    /**
     * 设置是否可取消以、动画以及显示位置
     * @param savedInstanceState Bundle
     * @return 对话框
     */
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setCancelable(cancelAble)
        dialog.setCanceledOnTouchOutside(outsideCancelAble)
        val lp = dialog.window?.attributes
        lp?.gravity = gravity
        dialog.window?.attributes = lp
        if (animStyle!==null){
            dialog.window?.attributes?.windowAnimations = animStyle
        }
        return dialog
    }

    override fun onStart() {
        super.onStart()
        val dm = DisplayMetrics()
        activity!!.windowManager.defaultDisplay.getMetrics(dm)
        dialog!!.window!!.setLayout(dialogWidth, dialogHeight)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (dialogWidth == 0 || dialogHeight == 0){
            dismiss()
            return
        }
        dialog?.window!!.attributes.dimAmount = dimAmount
        initView()
        loadData(true)
    }
}