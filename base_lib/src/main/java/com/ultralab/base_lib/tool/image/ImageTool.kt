package com.ultralab.base_lib.tool.image

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.ultralab.base_lib.tool.ActivityMgr
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import kotlin.math.roundToInt



/**
 * Bitmap模糊处理
 * @receiver 待处理的Bitmap
 * @param context 上下文的对象，默认为ActivityMgr.getContext()
 * @param scaleRate Float 压缩比例，默认为不进行压缩
 * @param blurRadius Float 模糊度，默认为15
 * @return Bitmap 处理后的Bitmap
 */
@SuppressLint("NewApi")
fun Bitmap.blur(context: Context = ActivityMgr.getContext(), scaleRate: Float = 1.0f, blurRadius: Float = 15f): Bitmap {

    val width = (this.width * scaleRate).roundToInt()
    val height = (this.height * scaleRate).roundToInt()

    val inputBitmap = Bitmap.createScaledBitmap(this, width, height, false)

    val outputBitmap = Bitmap.createBitmap(inputBitmap)

    val rs = RenderScript.create(context)

    val blurScript = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs))

    val tmpIn = Allocation.createFromBitmap(rs, inputBitmap)
    val tmpOut = Allocation.createFromBitmap(rs, outputBitmap)

    blurScript.setRadius(blurRadius)

    blurScript.setInput(tmpIn)

    blurScript.forEach(tmpOut)

    tmpOut.copyTo(outputBitmap)

    return outputBitmap
}

/**
 * 将Bitmap资源存储到本地
 * @receiver Bitmap 图片资源
 * @param context Context Context对象
 * @param compress Boolean 是否压缩
 * @return String 本地路径
 */
fun Bitmap.saveLocal(context:Context,compress:Boolean=false):String{
    //生成路径
    val picDir = File(context.filesDir, "pic")
    if (!picDir.exists()) {
        picDir.mkdirs()
    }
    //文件名为时间
    val timeStamp = System.currentTimeMillis()
    val fileName = "$timeStamp.jpg"
    //获取文件
    val file = File(picDir, fileName)
    var fos: FileOutputStream? = null
    var savaPath = file.path
    try {
        fos = FileOutputStream(file)
        if (compress){
            fos.write(this.toBytes(1000))
        }else{
            fos.write(this.toBytes(-1))
        }
        fos.flush()
        fos.close()
    } catch (e: Exception) {
        e.printStackTrace()
        savaPath = ""
    } finally {
        try {
            fos?.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    return savaPath
}

/**
 * 将Bitmap压缩并转换为ByteArray
 * @param maxKB 压缩后最大值，如果小于0表示不压缩
 */
fun Bitmap.toBytes(maxKB: Int = -1): ByteArray {
    val output = ByteArrayOutputStream()
    this.compress(Bitmap.CompressFormat.JPEG, 100, output)
    var options = 100
    if (maxKB>0){
        label@ while (output.toByteArray().size > maxKB * 1024) {
            output.reset() //清空output
            when (options){
                100->options=50
                50->options=20
                20->options=10
                10->options=6
                6->options=2
                2->options=1
                1->{break@label}
            }
            this.compress(Bitmap.CompressFormat.JPEG, options, output)
        }
    }else{
        this.compress(Bitmap.CompressFormat.JPEG, options, output)
    }
    return output.toByteArray()
}

/**
 * 获取默认图片加载配置
 * @return 图片加载配置
 */
fun defaultConfig(): ImageLoadConfig = ImageLoadConfig.Builder().build()

/**
 * 快速加载图片资源
 * @receiver ImageView
 * @param data 图片资源
 * @param config 图片加载配置
 */
fun ImageView.load(data:Any,config: ImageLoadConfig = defaultConfig()) = ImageLoader.getGlideInstance()
    .load(this,data,config)

/**
 * url 地址转 bitmap
 * 建议开启携程去使用该方法
 * @param context 上下文
 * */
fun String.asBitmap(context: Context): Bitmap? {
    return if (this.isNotEmpty()) {
        try {
            Glide.with(context).asBitmap().load(this).submit().get()
        } catch (e: java.lang.Exception) {
            null
        }
    } else {
        null
    }
}