package com.ultralab.base_lib.tool.image

import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import com.ultralab.base_lib.tool.ActivityMgr

/**
 * 图片加载配置
 * @author mazeyu
 * @email mazeyu8665@dingtalk.com
 * @date 2021/2/26
 * @since 1.0.0
 * @version 1.0.0
 */
class ImageLoadConfig(builder: Builder) {

    companion object {
        /**
         * 默认圆角度数3dp
         */
        const val DEFAULT_CORNER_RADIUS = 3
    }

    //裁剪类型
    var cropType = CropType.CENTER_CROP
        private set

    //加载类型
    var loadType = LoadType.NORMAL
        private set

    //加载优先级
    var priority = Priority.NORMAL
        private set

    //占位图
    var placeholder: Drawable? = null
        private set

    //错误图片
    var errorImage: Drawable? = null
        private set

    //圆角度数
    var corner: Int = DEFAULT_CORNER_RADIUS
        private set

    //圆角显示位置
    var cornerPosition: Array<CornerPosition>? = null
        private set

    //硬盘缓存,默认为all类型
    var diskCacheStrategy: DiskCache = DiskCache.ALL
        private set

    //使用内存缓存
    var useMemoryCache: Boolean = true
        private set

    //图片最终显示在ImageView上的宽高像素
    var overrideSize: OverrideSize? = null
        private set

    //设置缩略图的缩放比例0.0f-1.0f,如果缩略图比全尺寸图先加载完，就显示缩略图，否则就不显示
    var thumbnail = 0f
        private set

    //设置缩略图的url,如果缩略图比全尺寸图先加载完，就显示缩略图，否则就不显示
    var thumbnailUrl: String? = null
        private set

    //true,强制显示为常规图片,如果是gif资源,则加载第一帧作为图片
    var asBitmap = false
        private set

    //是否进行灰度处理
    var grayScale = false
        private set

    //是否进行高斯模糊处理
    var blur = false
        private set

    //唯一标识
    var tag: String? = null
        private set


    /**
     * 裁剪类型
     */
    enum class CropType {
        CENTER_CROP,//中间裁剪
        NORMAL//适合居中
    }

    /**
     * 加载类型
     */
    enum class LoadType {
        NORMAL, //普通
        CIRCLE, //圆形
        CORNER,//圆角
    }

    /**
     * 加载优先级
     */
    enum class Priority {
        HIGH,//高
        NORMAL,//中
        LOW//低
    }

    /**
     * 圆角显示位置
     */
    enum class CornerPosition {
        TOP_LEFT,//左上
        TOP_RIGHT,//右上
        BOTTOM_LEFT,//左下
        BOTTOM_RIGHT//右下
    }

    /**
     * 缓存模式
     */
    enum class DiskCache {
        NONE,//跳过硬盘缓存
        SOURCE,//仅仅保存原始分辨率的图片
        RESULT,//仅仅缓存最终分辨率的图像(降低分辨率后的或者转换后的)
        ALL//缓存所有版本的图片,默认
    }

    /**
     * 图片最终显示在ImageView上的宽高像素
     * @property width 宽
     * @property height 高
     */
    class OverrideSize(val width: Int, val height: Int)

    class Builder {
        //裁剪类型
        var cropType = CropType.CENTER_CROP

        //加载类型
        var loadType = LoadType.NORMAL

        //加载优先级
        var priority = Priority.NORMAL

        //占位图
        var placeholder: Drawable? = null

        //错误图片
        var errorImage: Drawable? = null

        //圆角度数
        var corner: Int = DEFAULT_CORNER_RADIUS

        //圆角显示位置
        var cornerPosition: Array<CornerPosition>? = null

        //硬盘缓存,默认为all类型
        var diskCacheStrategy: DiskCache = DiskCache.ALL

        //使用内存缓存
        var useMemoryCache: Boolean = true

        //图片最终显示在ImageView上的宽高像素
        var overrideSize: OverrideSize? = null

        //设置缩略图的缩放比例0.0f-1.0f,如果缩略图比全尺寸图先加载完，就显示缩略图，否则就不显示
        var thumbnail = 0f

        //设置缩略图的url,如果缩略图比全尺寸图先加载完，就显示缩略图，否则就不显示
        var thumbnailUrl: String? = null

        //true,强制显示为常规图片,如果是gif资源,则加载第一帧作为图片
        var asBitmap = false

        //是否进行灰度处理
        var grayScale = false

        //是否进行高斯模糊处理
        var blur = false

        //唯一标识
        var tag: String? = null

        /**
         * 设置裁剪类型
         * @param cropType 裁剪类型
         * @return Builder
         */
        fun setCropType(cropType: CropType): Builder {
            this.cropType = cropType
            return this
        }

        /**
         * 设置加载类型
         * @param loadType 加载类型
         * @return Builder
         */
        fun setLoadType(loadType: LoadType): Builder {
            this.loadType = loadType
            return this
        }

        /**
         * 设置加载优先级
         * @param priority 加载优先级
         * @return Builder
         */
        fun setPriority(priority: Priority): Builder {
            this.priority = priority
            return this
        }

        /**
         * 设置占位图
         * @param placeholder 占位图Drawable
         * @return Builder
         */
        fun placeholder(placeholder: Drawable?): Builder {
            this.placeholder = placeholder
            return this
        }

        /**
         * 设置占位图
         * @param res 占位图资源文件
         * @return Builder
         */
        fun placeholder(res: Int): Builder {
            this.placeholder = ContextCompat.getDrawable(ActivityMgr.getContext(), res)
            return this
        }

        /**
         * 设置错误图片
         * @param errorImage 错误图片Drawable
         * @return Builder
         */
        fun errorImage(errorImage: Drawable?): Builder {
            this.errorImage = errorImage
            return this
        }

        /**
         * 设置错误图片
         * @param res 错误图片资源文件
         * @return Builder
         */
        fun errorImage(res: Int): Builder {
            this.errorImage = ContextCompat.getDrawable(ActivityMgr.getContext(), res)
            return this
        }

        /**
         * 设置圆角度数
         * @param corner 圆角度数
         * @return Builder
         */
        fun setCorner(corner: Int): Builder {
            this.corner = corner
            return this
        }

        /**
         * 设置圆角显示位置
         * @param cornerPosition 圆角显示位置
         * @return Builder
         */
        fun setCornerPosition(cornerPosition: Array<CornerPosition>): Builder {
            this.cornerPosition = cornerPosition
            return this
        }

        /**
         * 设置硬盘缓存
         * @param diskCacheStrategy 硬盘缓存类型
         * @return Builder
         */
        fun setDiskCacheStrategy(diskCacheStrategy: DiskCache): Builder {
            this.diskCacheStrategy = diskCacheStrategy
            return this
        }

        fun useMemoryCache(useMemoryCache:Boolean): Builder {
            this.useMemoryCache=useMemoryCache
            return this
        }

        /**
         * 设置图片最终显示在ImageView上的宽高像素
         * @param overrideSize 图片最终显示在ImageView上的宽高像素
         * @return Builder
         */
        fun setOverrideSize(overrideSize: OverrideSize): Builder {
            this.overrideSize = overrideSize
            return this
        }

        /**
         * 设置缩略图的缩放比例
         * @param thumbnail 缩略图的缩放比例
         * @return Builder
         */
        fun setThumbnail(thumbnail: Float): Builder {
            this.thumbnail = thumbnail
            return this
        }

        /**
         * 设置缩略图的url
         * @param thumbnailUrl 缩略图的url
         * @return Builder
         */
        fun setThumbnailUrl(thumbnailUrl: String): Builder {
            this.thumbnailUrl = thumbnailUrl
            return this
        }


        /**
         * 设置是否强制显示为Bitmap
         * @param asBitmap 是否强制显示为Bitmap
         * @return Builder
         */
        fun setAsBitmap(asBitmap: Boolean): Builder {
            this.asBitmap = asBitmap
            return this
        }

        /**
         * 设置是否进行灰度处理
         * @param grayScale 是否进行灰度处理
         * @return Builder
         */
        fun setGrayScale(grayScale: Boolean): Builder {
            this.grayScale = grayScale
            return this
        }

        /**
         * 设置是否进行高斯模糊处理
         * @param blur 是否进行高斯模糊处理
         * @return Builder
         */
        fun setBlur(blur: Boolean): Builder {
            this.blur = blur
            return this
        }

        /**
         * 设置唯一标识
         * @param tag 唯一标识
         * @return Builder
         */
        fun setTag(tag: String): Builder {
            this.tag = tag
            return this
        }

        /**
         * 创建ImageLoadConfig
         * @return ImageLoadConfig
         */
        fun build(): ImageLoadConfig {
            return ImageLoadConfig(this)
        }

    }

    init {
        cropType = builder.cropType
        loadType = builder.loadType
        priority = builder.priority
        placeholder = builder.placeholder
        errorImage = builder.errorImage
        corner = builder.corner
        cornerPosition = builder.cornerPosition
        diskCacheStrategy = builder.diskCacheStrategy
        useMemoryCache = builder.useMemoryCache
        overrideSize = builder.overrideSize
        thumbnail = builder.thumbnail
        thumbnailUrl = builder.thumbnailUrl
        asBitmap = builder.asBitmap
        grayScale = builder.grayScale
        blur = builder.blur
        tag = builder.tag
    }
}