package com.ultralab.base_lib.tool.image

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Build
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.Transformation
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.ultralab.base_lib.tool.ActivityMgr
import com.ultralab.base_lib.tool.dp2px
import jp.wasabeef.glide.transformations.BlurTransformation
import jp.wasabeef.glide.transformations.GrayscaleTransformation
import jp.wasabeef.glide.transformations.RoundedCornersTransformation
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


/**
 * @author mazeyu
 * @email mazeyu8665@dingtalk.com
 * @date 2021/3/1
 * @since 1.0.0
 * @version 1.0.0
 */
class GlideLoader : ImageLoader() {

    /**
     * 加载图片资源
     * @param view ImageView
     * @param loadData 图片资源
     * @param config 图片加载配置
     * @param listener 普通加载回调监听
     * @param bitmapListener Bitmap回调监听
     */
    @SuppressLint("CheckResult")
    override fun load(
        view: ImageView,
        loadData: Any,
        config: ImageLoadConfig,
        listener: ImageLoadingListener?,
        bitmapListener: BitmapLoadingListener?
    ) {
        val context = if (view.context == null) {
            ActivityMgr.getContext()
        } else {
            view.context
        }
        if (context==null){
            return
        }else{
            if (if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    context is Activity && context.isDestroyed
                } else {
                    TODO("VERSION.SDK_INT < JELLY_BEAN_MR1")
                }
            ){
                return
            }
        }
        if (view.tag == loadData){
            return
        }
        view.tag = loadData
        if (config.asBitmap) {
            val builder = Glide.with(context).asBitmap().load(loadData)
            //配置缩略图
            if (config.thumbnail > 0) {
                //配置缩略图的缩放比例,如果为默认值0无缩放
                builder.thumbnail(config.thumbnail)
            } else {
                //配置url缩略图
                config.thumbnailUrl?.let {
                    builder.thumbnail(Glide.with(context).asBitmap().load(it))
                }
            }
            if (listener != null || bitmapListener != null) {
                builder.listener(object : RequestListener<Bitmap> {
                    override fun onResourceReady(
                        resource: Bitmap?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Bitmap>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        if (resource == null) {
                            listener?.onError()
                            bitmapListener?.onError()
                        } else {
                            listener?.onSuccess()
                            bitmapListener?.onSuccess(resource)
                        }
                        return false
                    }

                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Bitmap>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        listener?.onError()
                        bitmapListener?.onError()
                        return false
                    }
                })
            }
            builder.apply(generateRequestOptions(context, config)).into(view)
        } else {
            val builder = Glide.with(context).load(loadData)
            //配置缩略图
            if (config.thumbnail > 0) {
                //配置缩略图的缩放比例,如果为默认值0无缩放
                builder.thumbnail(config.thumbnail)
            } else {
                //配置url缩略图
                config.thumbnailUrl?.let {
                    builder.thumbnail(Glide.with(context).load(it))
                }
            }
            if (listener != null) {
                builder.listener(object : RequestListener<Drawable> {
                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {

                        listener.onSuccess()
                        return false
                    }

                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        listener.onError()
                        return false
                    }
                })
            }
//            builder.apply(generateRequestOptions(context, config)).into(view)
            if (config.placeholder==null){
                builder.apply(generateRequestOptions(context, config)).into(view)
            }else{
                builder.apply(generateRequestOptions(context, config)).thumbnail(
                    Glide.with(context).load(config.placeholder)
                        .apply(generateRequestOptions(context, config))
                ).into(view)
            }
        }
    }

    /**
     * 清除缓存
     */
    override fun clearCache() {
        GlobalScope.launch {
            Glide.get(ActivityMgr.getContext()).clearDiskCache()
        }
    }

    /**
     * 根据自定义图片加载配置生成glide加载请求
     * @param context 上下文对象
     * @param config 图片加载配置
     * @return glide加载请求
     */
    @SuppressLint("CheckResult")
    private fun generateRequestOptions(context: Context, config: ImageLoadConfig): RequestOptions {
        val requestOptions = RequestOptions()
        //配置加载优先级
        requestOptions.priority(
            when (config.priority) {
                ImageLoadConfig.Priority.HIGH -> Priority.HIGH
                ImageLoadConfig.Priority.NORMAL -> Priority.NORMAL
                ImageLoadConfig.Priority.LOW -> Priority.LOW
            }
        )
        //配置占位图
        config.placeholder?.let {
            requestOptions.placeholder(it)
        }
        //配置错误图片
        config.errorImage?.let {
            requestOptions.error(it)
        }
        //配置硬盘缓存
        requestOptions.diskCacheStrategy(
            when (config.diskCacheStrategy) {
                ImageLoadConfig.DiskCache.ALL -> DiskCacheStrategy.ALL
                ImageLoadConfig.DiskCache.RESULT -> DiskCacheStrategy.RESOURCE
                ImageLoadConfig.DiskCache.SOURCE -> DiskCacheStrategy.DATA
                ImageLoadConfig.DiskCache.NONE -> DiskCacheStrategy.NONE
            }
        )
        if (!config.useMemoryCache){
            requestOptions.skipMemoryCache(true)
        }
        //配置图片最终显示在ImageView上的宽高像素
        config.overrideSize?.let {
            requestOptions.override(it.width, it.height)
        }
        //配置唯一标识
        if (!config.tag.isNullOrEmpty()) {
            requestOptions.signature(GlideUrl(config.tag))
        }
        //添加transformation，实现圆角，模糊等各种效果
        val transformationList = arrayListOf<Transformation<Bitmap>>()
        when (config.loadType) {
            //添加圆形效果
            ImageLoadConfig.LoadType.CIRCLE -> transformationList.add(CircleCrop())
            //添加圆角效果
            ImageLoadConfig.LoadType.CORNER -> {
                transformationList.add(
                    RoundedCornersTransformation(
                        config.corner.dp2px(context), 0,
                        if (config.cornerPosition.isNullOrEmpty()) {
                            RoundedCornersTransformation.CornerType.ALL
                        } else {
                            val corners = config.cornerPosition ?: arrayOf()
                            when {
                                corners.contains(ImageLoadConfig.CornerPosition.TOP_LEFT)
                                        && corners.contains(ImageLoadConfig.CornerPosition.TOP_RIGHT)
                                        && corners.contains(ImageLoadConfig.CornerPosition.BOTTOM_LEFT)
                                        && corners.contains(ImageLoadConfig.CornerPosition.BOTTOM_RIGHT)
                                -> RoundedCornersTransformation.CornerType.ALL
                                corners.contains(ImageLoadConfig.CornerPosition.TOP_LEFT)
                                        && corners.contains(ImageLoadConfig.CornerPosition.TOP_RIGHT)
                                        && corners.contains(ImageLoadConfig.CornerPosition.BOTTOM_LEFT)
                                -> RoundedCornersTransformation.CornerType.OTHER_BOTTOM_RIGHT
                                corners.contains(ImageLoadConfig.CornerPosition.TOP_LEFT)
                                        && corners.contains(ImageLoadConfig.CornerPosition.TOP_RIGHT)
                                        && corners.contains(ImageLoadConfig.CornerPosition.BOTTOM_RIGHT)
                                -> RoundedCornersTransformation.CornerType.OTHER_BOTTOM_LEFT
                                corners.contains(ImageLoadConfig.CornerPosition.TOP_LEFT)
                                        && corners.contains(ImageLoadConfig.CornerPosition.BOTTOM_LEFT)
                                        && corners.contains(ImageLoadConfig.CornerPosition.BOTTOM_RIGHT)
                                -> RoundedCornersTransformation.CornerType.OTHER_TOP_RIGHT
                                corners.contains(ImageLoadConfig.CornerPosition.TOP_RIGHT)
                                        && corners.contains(ImageLoadConfig.CornerPosition.BOTTOM_LEFT)
                                        && corners.contains(ImageLoadConfig.CornerPosition.BOTTOM_RIGHT)
                                -> RoundedCornersTransformation.CornerType.OTHER_TOP_LEFT
                                corners.contains(ImageLoadConfig.CornerPosition.TOP_LEFT)
                                        && corners.contains(ImageLoadConfig.CornerPosition.TOP_RIGHT)
                                -> RoundedCornersTransformation.CornerType.TOP
                                corners.contains(ImageLoadConfig.CornerPosition.TOP_LEFT)
                                        && corners.contains(ImageLoadConfig.CornerPosition.BOTTOM_LEFT)
                                -> RoundedCornersTransformation.CornerType.LEFT
                                corners.contains(ImageLoadConfig.CornerPosition.TOP_LEFT)
                                        && corners.contains(ImageLoadConfig.CornerPosition.BOTTOM_RIGHT)
                                -> RoundedCornersTransformation.CornerType.DIAGONAL_FROM_TOP_LEFT
                                corners.contains(ImageLoadConfig.CornerPosition.TOP_RIGHT)
                                        && corners.contains(ImageLoadConfig.CornerPosition.BOTTOM_LEFT)
                                -> RoundedCornersTransformation.CornerType.DIAGONAL_FROM_TOP_RIGHT
                                corners.contains(ImageLoadConfig.CornerPosition.TOP_RIGHT)
                                        && corners.contains(ImageLoadConfig.CornerPosition.BOTTOM_RIGHT)
                                -> RoundedCornersTransformation.CornerType.RIGHT
                                corners.contains(ImageLoadConfig.CornerPosition.BOTTOM_LEFT)
                                        && corners.contains(ImageLoadConfig.CornerPosition.BOTTOM_RIGHT)
                                -> RoundedCornersTransformation.CornerType.BOTTOM
                                corners.contains(ImageLoadConfig.CornerPosition.TOP_LEFT) ->
                                    RoundedCornersTransformation.CornerType.TOP_LEFT
                                corners.contains(ImageLoadConfig.CornerPosition.TOP_RIGHT) ->
                                    RoundedCornersTransformation.CornerType.TOP_RIGHT
                                corners.contains(ImageLoadConfig.CornerPosition.BOTTOM_LEFT) ->
                                    RoundedCornersTransformation.CornerType.BOTTOM_LEFT
                                corners.contains(ImageLoadConfig.CornerPosition.BOTTOM_RIGHT) ->
                                    RoundedCornersTransformation.CornerType.BOTTOM_RIGHT
                                else ->
                                    RoundedCornersTransformation.CornerType.ALL
                            }
                        }
                    )
                )
            }
            else -> {
            }
        }
        //添加灰度处理效果
        if (config.grayScale) transformationList.add(0, GrayscaleTransformation())
        //添加高斯模糊处理效果
        if (config.blur) transformationList.add(0, BlurTransformation())
        //配置裁剪类型
        if (config.cropType == ImageLoadConfig.CropType.CENTER_CROP) {
            if (transformationList.size == 0) {
                requestOptions.centerCrop()
            } else {
                transformationList.add(0, CenterCrop())
                requestOptions.transform(MultiTransformation(transformationList))
            }
        } else {
            requestOptions.fitCenter()
        }
        return requestOptions
    }
}
