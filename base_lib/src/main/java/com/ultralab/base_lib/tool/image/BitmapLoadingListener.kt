package com.ultralab.base_lib.tool.image

import android.graphics.Bitmap

/**
 * Bitmap加载监听
 * @author mazeyu
 * @email mazeyu8665@dingtalk.com
 * @date 2021/2/26
 * @since 1.0.0
 * @version 1.0.0
 */
interface BitmapLoadingListener {
    /**
     * 成功回调
     * @param bitmap Bitmap图片
     */
    fun onSuccess(bitmap: Bitmap)
    /**
     * 失败回调
     */
    fun onError()
}