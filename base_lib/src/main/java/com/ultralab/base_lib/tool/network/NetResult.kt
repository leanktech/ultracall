package com.ultralab.base_lib.tool.network

import com.ultralab.base_lib.base.BaseResponse


data class NetResult<T> (val message: String, val code: Int, val source: T) :
    BaseResponse<T> {

    override fun code() = code

    override fun msg() = message

    override fun data() = source

    override fun isSuccess() = (code>0)&&(code%100==1)

}