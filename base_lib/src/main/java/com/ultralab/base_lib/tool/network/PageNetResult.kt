package com.ultralab.base_lib.tool.network

import com.ultralab.base_lib.base.BaseResponse


data class PageNetResult<T>(
    val message: String,
    val code: Int,
    val current: Int?,
    val pages: Int?,
    val size: Int?,
    val total: Int?,
    val rows: ArrayList<T>
) :
    BaseResponse<ArrayList<T>> {

    override fun code() = code

    override fun msg() = message

    override fun data() = rows

    override fun isSuccess() = (code % 100 == 1)

    fun existSurplusData() = (pages?:0)>(current?:0)

}